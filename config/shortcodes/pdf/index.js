const pdf = (url) => {
  return `<div class="pdf">
      <object type="application/pdf" data="${url}?#zoom=page-width&pagemode=none"><p>Il file non può essere visualizzato!</p></object>
    </div>`;
};
module.exports = pdf;
