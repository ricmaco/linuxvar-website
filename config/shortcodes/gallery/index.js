const Image = require('@11ty/eleventy-img');
const path = require('path');
const fs = require('fs/promises');
const htmlmin = require('html-minifier-terser');

/**
 * content should be in this form:
 * [
 *   {
 *      "source": "./path/to/image.jpg",
 *      "alt": "An image",
 *      "caption": "A very good image"
 *   },
 *   ...
 * ]
 */
const parseJson = (content) => {
    try {
        return JSON.parse(content);
    } catch (error) {
        throw new Error(`Invalid 'content' format: ${error}`);
    }
};

const formatFilename = (id, src, width, format, options) => {
    const extension = path.extname(src);
    const name = path.basename(src, extension);
    return `${name}-${width}w.${format}`;
};

const gallery = async (
    content,
    cls = '',
    sizes = '(min-width: 55rem) 820px, 100vw'
) => {
    const imagesJson = parseJson(content);

    const images = await Promise.all(imagesJson.map(async image => {
        const metadata = await Image(image.source, {
            widths: ['auto'],
            formats: ['webp'],
            urlPath: '/assets/images/',
            outputDir: './dist/assets/images/',
            filenameFormat: formatFilename,
        });

        return {
            ...image,
            metadata,
        }
    }));

    const galleryName = `gallery-${Math.random().toString(36).slice(2, 7)}`;

    return htmlmin.minify(`
        <div class="gallery ${cls}">
            ${images.map(image => {
                const metadata = image.metadata.webp[0];
                const biggestMetadata = image.metadata.webp[image.metadata.webp.length - 1];
                return `
                    <a class="glightbox"
                        href="${biggestMetadata.url}"
                        data-gallery="${galleryName}"
                        ${image.caption? `data-description="${image.caption}"`:''}
                    >
                        <figure>
                            <picture>
                            ${Object.values(image.metadata)
                                .map(imageFormat =>
                                    `<source type="${imageFormat[0].sourceType}" srcset="${imageFormat
                                        .map(entry => entry.srcset)
                                        .join(', ')}" sizes="${sizes}">`)
                                .join('\n')}
                            <img
                                src="/assets/images/image-placeholder.png"
                                data-src="${metadata.url}"
                                width="${metadata.width}"
                                height="${metadata.height}"
                                alt="${image.alt}"
                                loading = 'lazy'
                                decoding="async" />
                            </picture>
                        </figure>
                    </a>`;
            }).join('\n')}
    </div>`, {collapseWhitespace: true});
};

module.exports = gallery;
