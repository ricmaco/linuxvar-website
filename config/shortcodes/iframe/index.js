const iframe = (
    src,
    caption = '',
    captionLink = ''
) => `<div class="figure-container"><figure>
    <iframe src="${src}" scrolling="no" style="overflow: hidden;"></iframe>
    ${caption ? captionLink ? `<figcaption><a href="${captionLink}">${caption}</a></figcaption>` : `<figcaption><p>${caption}</p></figcaption>` : ''}
</figure></div>`;

module.exports = iframe;

