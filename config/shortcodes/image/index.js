const Image = require('@11ty/eleventy-img');
const path = require('path');
const htmlmin = require('html-minifier-terser');

const formatFilename = (id, src, width, format, options) => {
    const extension = path.extname(src);
    const name = path.basename(src, extension);
    return `${name}-${width}w.${format}`;
};

const image = async (
  src,
  alt,
  caption,
  cls = '',
  sizes = '(min-width: 55rem) 820px, 100vw'
) => {
  if (!alt) {
    throw new Error(`Missing \`alt\` on myImage from: ${src}`);
  }

  const metadata = await Image(src, {
    widths: ['auto'],
    formats: ['webp'],
    urlPath: '/assets/images/',
    outputDir: './dist/assets/images/',
    filenameFormat: formatFilename,
  });

  const biggestMetadata = metadata.webp[metadata.webp.length - 1];

  return htmlmin.minify(`
    <div class="figure-container">
        <figure>
            <picture>
                ${Object.values(metadata)
                    .map(imageFormat => {
                            return `<source type="${imageFormat[0].sourceType}" srcset="${imageFormat
                            .map(entry => entry.srcset)
                            .join(', ')}" sizes="${sizes}">`;
                        }).join('\n')
                    }
                <img
                    ${cls ? `class="${cls}"` : ''}
                    src="/assets/images/image-placeholder.png"
                    data-src="${biggestMetadata.url}"
                    width="${biggestMetadata.width}"
                    height="${biggestMetadata.height}"
                    alt="${alt}"
                    loading = 'lazy'
                    decoding="async">
            </picture>
            ${caption
                ? `<figcaption><p>${caption}</p></figcaption>`
                : ''
            }
        </figure>
    </div>`,
    {collapseWhitespace: true}
  );
};

module.exports = image;
