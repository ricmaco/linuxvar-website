---
title: "Nextion display, touchscreen LCD TFT"
date: "2019-01-16"
tags: 
  - "2019"
  - "mezzore"
  - "display"
  - "nextion"
  - "touchscreen"
---

Il display Nextion è una soluzione HMI (Human Machine Interface) ovvero un'interfaccia di controllo e visualizzazione tra un umano e un processo, una macchina, un'applicazione o uno strumento.

Felice si interessa di Arduino e ha cominciato ad interessarsi anche soluzioni grafiche e quindi ha tenuto una mezz'ora in amicizia proprio su questo display per la creazione di interfacce grafiche. La serata è stata interamente dedicata alla discussione su questo argomento. La presentazione è, come al solito, adatta ad ogni tipo di pubblico, soprattutto per i non addetti ai lavori.

{# FIXME: pdf file missing
{% pdf "/files/01/nextion-display-touchscreen-lcd-tft/files/Nextion_presentazione-1.pdf" %}
#}