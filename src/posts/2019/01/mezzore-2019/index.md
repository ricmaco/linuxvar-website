---
title: "Mezz'ore 2019"
date: "2019-01-01"
tags: 
  - "mezzore"
---

## Programma

Di seguito il programma per quest'anno; ogni data riporterà il collegamento alla pagina specifica per il contenuto. (Il programma potrebbe essere soggetto a variazioni)

- **16 Gennaio 2019:** _Felice F._, [Nextion Display Touch Intelligente](../nextion-display-touchscreen-lcd-tft)
- **30 Gennaio 2019:** _Riccardo F._, [WebMin](../webmin)
- **20 Febbraio 2019:** _Ross_, [VirtualMin](../../02/virtualmin)
- **6 Marzo 2019:**  Alberto, [Esp32 & IoT](../../03/esp32-iot)
