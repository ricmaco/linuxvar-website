---
title: "WebMin"
date: "2019-01-30"
tags: 
  - "2019"
  - "mezzore"
  - "webmin"
---

Webmin è un software che permette la gestione di un computer tramite interfaccia web.

Permette di gestire la macchina sia da un punto di vista hardware che software, ed è prevalentemente indirizzato all'uso da parte di amministratori e sistemisti.

È modulare e all'atto dell'installazione sono disponibili circa 116 moduli di default (terze parti hanno poi sviluppato altri moduli).

Riccardo F. ci farà vedere una situzione tipica.