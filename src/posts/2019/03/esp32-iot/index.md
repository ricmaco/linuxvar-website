---
title: "Esp32 & IoT"
date: "2019-03-06"
tags: 
  - "mezzore"
  - "esp32"
  - "iot"
---

Questa sera Alberto ci ha parlato di questo modulo della Espressif Systems, illustrandone le potenzialità e tutto quello che bisogna sapere su questa dev board per riuscire a sfruttarla al meglio.

## Presentazione

Di seguito la presentazione della serata.

{% pdf "/files/03/esp32-iot/files/LXvarESP32-1.pdf" %}

## Link utili

### Install tutorial

- https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions

### Cosa trovate gia' pronto :  

- https://github.com/arendst/Sonoff-Tasmota/wiki
- https://esphome.io/index.html
- https://github.com/1technophile/OpenMQTTGateway

### Setup

- https://forum.arduino.cc/index.php?topic=358374.0
- https://www.instructables.com/id/IOT-Made-Simple-Playing-With-the-ESP32-on-Arduino-

### Sitografia

- https://en.wikipedia.org/wiki/ESP32
- http://esp32.net/
- http://iot-bits.com/category/esp32-articles/
- https://www.esp32.com (forum inglese)
- https://leanpub.com/kolban-ESP32
- https://github.com/nkolban
- http://www.lucadentella.it/category/esp32

### Tutorial

- Andreas Spiess: https://www.youtube.com/channel/UCu7_D0o48KbfhpEohoP7YSQ
- Peter Scargill's Tech Blog: https://tech.scargill.net/Florian%201technophile, https://1technophile.blogspot.com/

## Download

È disponibile un [archivio compresso con tutti i file](/files/03/esp32-iot/files/LinuxVar-esp32show.zip).
