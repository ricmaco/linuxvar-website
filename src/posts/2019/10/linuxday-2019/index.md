---
title: "LinuxDay - Sabato 26 Ottobre 2019"
date: "2019-10-26"
tags: 
  - "linuxday"
  - "linuxday 2019"
  - "2019"
---

Il [Linux Day](http://www.linuxday.it/) è la principale manifestazione italiana dedicata alla promozione di GNU/Linux e del Software Libero. Quest'anno si terrà il 26 ottobre in numerose città italiane. L'edizione di Tradate sarà organizzata presso il [FaberLab](http://www.faberlab.org/) a Tradate da [LinuxVar](http://linuxvar.it/) e [LIFO](http://www.lifolab.org) e [GL-Como](https://www.gl-como.it).

Passare una mezza giornata giornata in compagnia del nostro futuro (gli studenti e i giovani), e non solo degli appassionati del settore, ci permette sia di portare avanti tutti insieme la causa del nostro tanto amato sistema operativo, sia di insegnare alle menti più giovani l'importanza di avere del software sviluppato attraverso piattaforme libere e soprattutto liberamente consultabile e modificabile.

Il pomeriggio verterà su presentazioni di carattere più tecnico, mirate a quella parte di voi più dedita all'informatica, come professionisti del settore, appassionati e, perchè no, studenti che vogliono approfondire la loro conoscenza nel campo Linux.

## Programma
* * *

### Mattino

- **09:00 - 12:30** ~ Lan party e momenti di relax

### Pomeriggio

- **14:30 - 14:50** ~ Saluti con le autorità locali
- **15:00 - 15:20** ~ Caramelle dagli sconosciuti - Ed altri buoni motivi per usare apt
- **15:30 - 15:50** ~ Da Python 2 a Python 3 senza ritorno
- **16:00 - 16:20** ~ Coffee break
- **16:30 - 16:50** ~ Blender 2.8
- **17:00 - 17:20** ~ Infilare a forza python in un'applicazione closed per salvaguardare la sanità mentale
- **17:30 - 17:50** ~ Ansible: the sane way

### Sera

- **19:15 - Ad oltranza** ~ Cena

## Come raggiungerci
* * *

Il Faberlab si trova a Tradate in viale Europa 4/A, lungo la Varesina, all'altezza della rotonda con l'aereo, sopra al supermercato Eurospin.

{% iframe
    "https://www.openstreetmap.org/export/embed.html?bbox=8.903313875198364%2C45.70220819617652%2C8.907465934753418%2C45.70465831658852&amp;layer=mapnik&amp;marker=45.70343326980482%2C8.905389904975891",
    "Vedi su OpenStreetMaps",
    "https://www.openstreetmap.org/?mlat=45.67149&mlon=8.74090#map=19/45.67149/8.74090"
%}