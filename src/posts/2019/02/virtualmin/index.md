---
title: "VirtualMin"
date: "2019-02-20"
tags: 
  - "2019"
  - "mezzore"
  - "virtualmin"
---

Virtualmin è un modulo per Webmin utile per amministrare da una singola interfaccia tutti i virtual hosts del nostro server, in maniera simile ai pannelli Plesk o Cpanel.

Supporta la creazione e l'amministrazione di virtual host Apache / Nginix, di database MySQL, di server DNS Bind e di mailbox con Sendmail o Postifx.

può inoltre gestire utenti Webmin separati per ogni virtual server, per restringere e separare i compiti amministrativi degli utenti.

In questa mezz'ora Ross esporrà la versione GPL.

Una serata molto incentrata sulla parte pratica.