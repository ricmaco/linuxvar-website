---
title: "Planet LUG"
date: "2021-01-04"
categories:
  - "link-utili"
  - "2021"
---

Ci segnalano "Planet LUG¨, un sito italiano che raccoglie molti articoli riferiti a Linux.

Visitalo su [https://planet.linux.it/lug/](https://planet.linux.it/lug/).
