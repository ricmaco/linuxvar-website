---
title: "Stampante 3D"
date: "2021-01-09"
tags: 
  - "tutorial"
  - "3d"
  - "stampante"
  - "stampa 3d"
  - "2021"
---

In sede abbiamo un anuova stampante 3D configurata e pronta per essere utilizzata. Si tratta di una ANET A8, un prodotto entry level ma che, nei primi test effettuati, si è dimostrata valida. Ovviamente è a disposizione di tutti i frequentatori del LinuxVar.

Per creare i file gcode utilizziamo il software Cura, scaricabile a [questo indirizzo](https://ultimaker.com/it/software/ultimaker-cura) ([archive.org mirror](https://web.archive.org/web/20200926141722/https://ultimaker.com/it/software/ultimaker-cura), in caso il link originale sparisse).

## Configurazione di Cura

Per aggiungere una nuova stampante, andare su "Impostazioni", "Stampante", "Aggiungi stampante", successivamente in "Other" scegliere "Prusa 13" e impostare i parametri come nell'immagine:

{% image
    "./src/posts/2021/01/stampante-3d/images/cura-settings.png",
    "cura settings",
    "Settaggi di Cura"
%}