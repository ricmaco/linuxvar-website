---
title: "Nuova Distro: Ufficio Zero Linux"
date: "2021-02-25"
tags: 
  - "tutorial"
  - "distro"
  - "ufficio-zero"
  - "2021"
---

Di seguito la mail di Julian Del Vecchio di UfficioZero Team che ci invita a provare questa distro e recensirla.

> Carissimi Rappresentanti dei Lug,
>
> vorrei presentarvi la rinascita del progetto italiano Ufficio Zero Linux.
>
> Ufficio Zero in origine è stata una distro live basata prima su Arch e poi su Ubuntu, dapprima intesa come live ed in seguito come sistema installabile.
> Dopo svariati anni di silenzio ed il suo abbandono, nell'aprile del 2020 abbiamo ripreso il progetto con l'obiettivo iniziale di fornire, in piena pandemia e restrizioni varie, uno strumento gratuito contenente svariate applicazioni preinstallate al suo interno, per dare la possibilità a liberi professionisti ma non solo, di poter lavorare dalle proprie abitazioni anche con hardware obsoleto.
>
> Dalla prima versione basata su xUbuntu 18.04 tante sono state le modifiche e migliorie, tanto da portare il nostro piccolo team alla creazione di svariate versioni che prendono il nome di alcune città italiane, anche per far conoscere il nostro splendido territorio all'estero, considerando che viene molto utilizzata in Europa ed in Giappone.
>
> Le versioni ora disponibili sono:
>
> - Roma - basata su Devuan 3 BeoWulf per hardware a 32bit con DM Xfce4
> - Mantova - basata su PCLinuxOS per hardware a 64bit con DM Mate e contenente anche software free
> - Vieste - basata su Linux Mint 19.3 per hardware a 64bit con DM Mate
> - Bergamo - basata su PCLinuxOS per hardware a 64bit con DM Mate e contenente per lo più software open source
> - Siena - basata su LMDE4 Debbie per hardware a 32bit e ottimizzata per netbook con DM Mate
> - Tropea - basata su Linux Mint 20 per hardware a 64bit con DM Mate ed aggiornabile all'ultima versione rilasciata con l'usabilità dei repository Ulyssa Mint 20.1
>
> Quello che c'è da sapere su Ufficio Zero è che non è una nuova distro in se, poichè si è cercato di prendere il meglio dei sistemi stabili su cui si basano le svariate versioni ed aggiungendo gli stessi temi, parco software preinstallato e proposto per rendere il sistema più user friendly, visto la diversificazione di sistemi linux e la loro deframmentazione.
>
> Vogliamo fornire una nuova esperienza di utilizzo dei sistemi desktop e proprio per questo, il nostro post-installer è suddiviso per categorie: ufficio, tecnica, sviluppo, ecc. ed ogni categoria propone software aggiuntivo da installare.
>
> Non mancano poi software di firma digitale (per ora solo ArubaSign), LBRY (contenitore decentralizzato per la pubblicazione e visualizzazione di video) ed altri strumenti che rendono le nostre versioni delle vere e proprie Out of the Box.
>
> Quello che oggi sono a chiedervi è un po di pubblicità, se possibile, sui vostri portali ed eventuali recensioni per permettere una maggiore visibilità al progetto Ufficio Zero.
>
> Per ogni altra informazione utile, potete basarvi sulle informazioni contenute sul sito all'indirizzo web [https://ufficiozero.org](https://ufficiozero.org)
>
> Vi ringrazio per la vostra disponibilità, scusandomi anticipatamente per il disturbo eventualmente arrecato, e vi auguro un buon weekend.
>
> -- 
> Julian Del Vecchio
>
> UfficioZero Team
