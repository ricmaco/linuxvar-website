---
title: "Nuova mailing list del LinuxVar presso ILS"
date: "2024-12-19"
tags: 
  - "incontri"
  - "comunicazione"
  - "gruppo"
  - "associazione"
  - "mailing-list"
  - "2024"
---

Con un messaggio sul nostro [gruppo Telegram](https://t.me/joinchat/QmTosPpV6Qo9QtyS), il nostro Jacopo è lieto di annunciare la nostra nuova mailing list:

> Ciao, abbiamo una nuova mailing list hostata su lists.linux.it, della ILS (a cui abbiamo di nuovo pensato di associarci, ma rimando eventuale discussione, casomai qualcuno volesse sollevarla, alla nuova scintillante ML).
> Prego iscrivetevi, soprattutto se avete domande e discussioni tecniche.
> https://lists.linux.it/listinfo/linuxvar

Come potete leggere nel messaggio, abbiamo scelto di associarci alla [ILS](https://www.ils.org), Italian Linux Society.

Se desiderate fare parte della mailing list, potete iscrivervi visitando [questo link](https://lists.linux.it/listinfo/linuxvar). Per l'occasione abbiamo anche aggiunto un'icona social su questo sito.