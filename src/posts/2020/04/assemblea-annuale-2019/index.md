---
title: "Assemblea Annuale 2019"
date: "2020-04-10"
tags: 
  - "incontri"
  - "assemblea"
  - "2020"
---

A partire dalle ore 22.00 del 10 Aprile si è svolta l'Assemblea Annuale del LinuxVar.

Al termine delle votazioni sono stati eletti:

- per la carica di Presidente:
  - Carabelli Gianni
- mentre hanno ottenuto voti per la carica di Consigliere:
  - Gaiatto Cristian, eletto consigliere e vicepresidente
  - Ferrazza Felice, eletto consigliere
  - Marmo Giuseppe, eletto consigliere
  - Bellin Lionello, eletto consigliere con delega

L'assemblea viene conclusa alle 22.45.
