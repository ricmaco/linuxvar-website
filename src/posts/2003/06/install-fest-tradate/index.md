---
title: "Install Fest Tradate"
date: "2003-06-29"
tags: 
  - "incontri"
  - "2003"
  - "install-fest"
  - "tradate"
gallery: true
---

Il LinuxVar, organizza Domenica 29 Giugno 2003 a Tradate la prima edizione del **Linux Install Fest**. Un evento libero e gratuito, durante il quale verranno effettuate installazioni del sistema operativo GNU/Linux sui pc dei partecipanti dai nostri soci volontari, che forniranno anche spiegazioni sul funzionamento e sull’utilizzo del software.

Porta il tuo computer (sia desktop o portatile) e riceverai assistenza all'installazione di Linux conservando il tuo attuale sistema operativo. Gratuitamente e liberamente.

Qui la [locandina](./images/Inst_fest_2003_locandina.png) della giornata.

## Come raggiungerci

{% iframe
    "https://www.openstreetmap.org/export/embed.html?bbox=8.885831236839296%2C45.650376496858385%2C8.889371752738954%2C45.65219142646449&amp;layer=mapnik&amp;marker=45.65128396901302%2C8.887601494789124",
    "Vedi su OpenStreetMaps",
    "https://www.openstreetmap.org/?mlat=45.65128&amp;mlon=8.88760#map=19/45.65128/8.88760"
%}

## Galleria Fotografica

{% gallery %}
[
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_1.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_10.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_11.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_12.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_13.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_14.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_15.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_16.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_17.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_18.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_19.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_2.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_20.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_21.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_22.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_3.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_4.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_5.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_6.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_7.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_8.jpg",
        "alt": "foto dell'install fest"
    },
    {
        "source": "./src/posts/2003/06/install-fest-tradate/images/Inst_Fest_2003_9.jpg",
        "alt": "foto dell'install fest"
    }
]
{% endgallery %}
