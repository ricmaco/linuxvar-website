---
title: "LinuxDay - Sabato 29 Novembre 2003"
date: "2003-11-29"
tags: 
  - "2003"
  - "linuxday"
  - "linuxday-2003"
gallery: true
---

La terza edizione del [LinuxDay](http://www.linuxday.it) apre all'insegna della continuità e del rinnovamento. Linux Day è un appuntamento annuale organizzato da varie realtà sparse sul territorio nazionale, dette LUG: Linux User Group. Si tratta di associazioni senza scopo di lucro volte a promuovere tanto il sistema operativo in sé, quanto la filosofia alla base di esso.

Il Linux Day nasce come evento che riunisce appassionati e curiosi, operatori del settore e professionisti ed è interessante notare come le edizioni passate abbiano goduto di una copertura mediatica al di là di ogni più rosea aspettativa. Gli obiettivi del Linux Day sono molteplici, ma si possono riassumere in due termini: sensibilizzazione e formazione. Sensibilizzazione verso qualcosa che è più di un pensiero alternativo, quanto una precisa risposta alle esigenze tanto dell'azienda, quanto dell'utente individuale, e formazione di competenze per aiutare verso il passaggio ad un sistema informatico alternativo.

Il **LinuxDay 2003** si è svolto presso la sede di Varese dell'[Università degli Studi dell'Insubria](http://www.uninsubria.it/) in via Ravasi 2.

Il [programma](./images/LD_2003_Locanda_Fox.jpg) e la locandina [1](./images/LD_2003_volantino_1.png) e [2](./images/LD_2003_volantino_2.png) della giornata

## Come raggiungerci

{% iframe
    "https://www.openstreetmap.org/export/embed.html?bbox=8.826758265495302%2C45.812357313482146%2C8.83029878139496%2C45.81416698694819&amp;layer=mapnik&amp;marker=45.813262157565696%2C8.82852852344513",
    "Vedi su OpenStreetMaps",
    "https://www.openstreetmap.org/?mlat=45.81326&mlon=8.82853#map=19/45.81326/8.82853"
%}

## Galleria Fotografica

{% gallery %}
[
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_1.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_10.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_100.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_101.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_102.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_103.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_104.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_105.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_106.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_107.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_108.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_109.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_11.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_110.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_111.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_112.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_113.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_114.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_115.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_116.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_117.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_119.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_12.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_120.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_121.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_122.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_123.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_124.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_125.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_127.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_128.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_129.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_13.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_130.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_131.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_132.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_133.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_134.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_135.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_136.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_137.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_138.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_139.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_14.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_140.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_141.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_142.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_143.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_144.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_145.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_146.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_147.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_148.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_149.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_15.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_150.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_151.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_152.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_153.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_154.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_155.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_156.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_157.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_158.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_159.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_16.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_160.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_161.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_162.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_163.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_164.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_165.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_166.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_167.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_168.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_169.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_17.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_170.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_171.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_172.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_173.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_174.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_175.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_176.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_177.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_178.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_179.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_18.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_182.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_183.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_185.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_186.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_19.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_2.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_20.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_21.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_22.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_23.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_24.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_25.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_26.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_27.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_28.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_29.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_3.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_30.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_31.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_32.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_33.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_34.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_35.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_36.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_37.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_38.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_39.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_4.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_40.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_41.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_42.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_43.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_44.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_45.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_46.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_47.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_48.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_49.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_5.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_50.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_51.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_52.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_53.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_54.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_55.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_56.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_57.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_58.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_59.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_6.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_60.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_61.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_62.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_63.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_64.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_65.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_66.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_67.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_68.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_69.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_7.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_70.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_71.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_72.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_73.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_74.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_75.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_76.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_77.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_78.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_79.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_8.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_80.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_81.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_82.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_83.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_84.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_9.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_95.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_96.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_97.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_98.jpg",
        "alt": "foto del linuxday"
    },
    {
        "source": "./src/posts/2003/11/linuxday-2003/images/LD_2003_99.jpg",
        "alt": "foto del linuxday"
    }
]
{% endgallery %}
