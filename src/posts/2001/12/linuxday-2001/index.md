---
title: "LinuxDay - Sabato 1 Dicembre 2001"
date: "2001-12-01"
tags:
  - "linuxday"
  - "linuxday-2001"
  - "2001"
---

In occasione del 1° Linux Day, organizzato a livello nazionale dalla [Italian Linux Society](http://www.linux.it), il LinuxVar partecipa a questo evento grazie agli spazi gentilmente messi a disposizione dal Comune di Sesto Calende, in cui potrete incontrare i vari membri del LinuxVar, chiacchierare con loro, esporre problemi e iniziative; seguire le conferenze che abbiamo approntato per voi e vedere qualche macchina dimostrativa.
