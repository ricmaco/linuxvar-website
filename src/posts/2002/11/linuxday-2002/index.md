---
title: "LinuxDay - Sabato 23 Novembre 2002"
date: "2002-11-23"
tags: 
  - "linuxday"
  - "linuxday-2002"
  - "2002"
gallery: true
---

Dopo il successo dell’edizione 2001, la [Italian Linux Society](https://www.linuxday.it/) ha indetto per il prossimo 23 novembre il Linux Day 2002, seconda giornata nazionale dedicata a GNU/Linux e al Software Libero. Si tratta di un evento “distribuito” in tutta Italia, con manifestazioni in tutte le città dove è presente un LUG (Linux User Group) e organizzate dai singoli gruppi locali.

È una manifestazione pubblica senza scopo di lucro: i singoli eventi locali si terranno in luoghi aperti a chiunque vi voglia partecipare. Le tematiche che verranno portate a contatto con il pubblico saranno di ordine tecnico, legale e filosofico, sempre riguardo a GNU/Linux e al Software Libero.

Il LinuxVar e gli amici del [Lifo](https://www.lifolab.org/) di Varese organizzano il Linux Day a Solbiate Olona dalle 10:00 alle 19:00 presso C.A.G. Qui [locandina](./images/LD_2002_locandina.png) e [programma](./images/LD_2002_volantino.png) della giornata.

## Come raggiungerci

{% iframe
    "https://www.openstreetmap.org/export/embed.html?bbox=8.885831236839296%2C45.650376496858385%2C8.889371752738954%2C45.65219142646449&amp;layer=mapnik&amp;marker=45.65128396901302%2C8.887601494789124",
    "Vedi su OpenStreetMaps",
    "https://www.openstreetmap.org/?mlat=45.65128&amp;mlon=8.88760#map=19/45.65128/8.88760"
%}

## Galleria fotografica

{% gallery %}
[
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_10.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_11.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_12.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_13.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_14.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_15.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_16.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_17.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_18.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_19.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_2.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_20.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_21.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_22.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_23.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_24.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_25.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_26.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_27.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_28.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_29.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_3.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_30.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_31.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_32.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_33.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_34.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_35.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_36.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_37.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_38.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_39.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_4.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_40.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_41.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_42.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_43.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_44.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_45.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_46.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_5.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_6.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_7.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_8.jpg",
        "alt": "foto del linuxvar 2002"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_9.jpg",
        "alt": "foto del linuxvar 2002"
    }
]
{% endgallery %}

## Dicono di noi

{% gallery %}
[
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_Prealpina_3.jpg",
        "alt": "foto della prealpina",
        "caption": "La prealpina"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_Prealpina_2.jpg",
        "alt": "foto della prealpina",
        "caption": "La prealpina"
    },
    {
        "source": "./src/posts/2002/11/linuxday-2002/images/LD_2002_Prealpina_1.jpg",
        "alt": "foto della prealpina",
        "caption": "La prealpina"
    }
]
{% endgallery %}
