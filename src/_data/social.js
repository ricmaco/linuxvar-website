module.exports = {
  widget: [
    {
        "platform": 'email',
        "url": 'mailto:linuxvar06@gmail.com',
        "class": 'mail',
        "icon": 'las la-at'
    },
    {
        "platform": 'telegram',
        "url": 'https://t.me/joinchat/QmTosPpV6Qo9QtyS',
        "class": 'telegram',
        "icon": 'lab la-telegram-plane'
    },
    {
        "platform": 'mailing-list',
        "url": 'https://lists.linux.it/pipermail/linuxvar',
        "class": 'mailing-list',
        "icon": 'las la-mail-bulk'
    },
    {
        "platform": 'youtube',
        "url": 'https://www.youtube.com/channel/UCoYyCUclcvKVnvyfgla4I0g',
        "class": 'youtube',
        "icon": 'lab la-youtube'
    },
    {
        "platform": 'facebook',
        "url": 'https://www.facebook.com/groups/35265137952',
        "class": 'facebook',
        "icon": 'lab la-facebook'
    }
  ],
  footer: [
    {
      "platform": "github",
      "url": "https://gitlab.com/ricmaco/linuxvar.eu",
      "icon": "lab la-github"
    },
    {
      "platform": "rss",
      "url": "/feed.xml",
      "icon": "las la-rss"
    }
  ]
};
