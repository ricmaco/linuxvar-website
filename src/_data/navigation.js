module.exports = {
  top: [
    {
      text: 'Home',
      url: '/'
    },
    {
      text: 'Chi Siamo',
      url: '/about/'
    },
    {
      text: 'Attività',
      url: '/activities/'
    },
    {
      text: 'Articoli',
      url: '/posts/'
    }
  ],
  bottom: [
    {
      text: 'Contatti',
      url: '/about/#contatti'
    },
    {
      text: 'Privacy',
      url: '/privacy/'
    }
  ]
};
