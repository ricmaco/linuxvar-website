module.exports = [
    {
        icon: "las la-hourglass-half",
        title: "Mezz'ore d'Amicizia",
        text: [
            "C'è sempre qualcuno ne sa più di noi. Come da tradizione del mondo libero, la conoscenza va condivisa: da quest'idea nascono le mezz'ore d'amicizia."
        ],
        link: {
            text: "Guarda",
            url: "/activities/#mezzore"
        }
    },
    {
        icon: "lab la-linux",
        title: "LinuxDay",
        text: [
            "Dal 2001 il Linux Day è la principale iniziativa distribuita per conoscere ed approfondire Linux ed il Software Libero."
        ],
        link: {
            text: "Scopri",
            url: "/activities/#linuxday"
        }
    },
    {
        icon: "lar la-handshake",
        title: "Incontri",
        text: [
            "Il LinuxVar tiene molte attività sul territorio: da incontri pratici per la configurazione di Linux, ad altri mirati ad ampliare le conoscenze in modo socievole e reciproco."
        ],
        link: {
            text: "Partecipa",
            url: "/activities/#incontri"
        }
    }
];
