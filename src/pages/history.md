---
title: Storia degli inizi del LinuxVar
permalink: /history/index.html
description: Storia dei primi anni di genesi del LinuxVar
layout: page
---

## Prima: il MILUG

Il LinuxVar esiste come gruppo organizzato dall'anno 2001 e si ricorda la presenza dei primi sparuti personaggi già nel corso dell’anno precedente.
Durante quel periodo alcuni baldi giovanotti della nostra provincia frequentavano la mailing list del MILUG, il LUG della provincia di Milano e qualcuno di loro, un bel
giorno, ci si pose la domanda fatale: "Perché Milano sì e a Varese no?". Detto fatto.

Grazie alla disponibilità di Ziobudda, venne inviata una mail a quel LUG alla ricerca di persone interessate alla nascita della nuova creatura. E fu così che, nella data astrale del 16 luglio 2000, venne creata la storica mailing list, adesso non più esistente. O, almeno, ciò è quello che si evince dal diario di bordo.

## La genesi

I primi incontri avvenivano esclusivamente per mezzo di questa nuova mailing list, strumento che rivelò la sua utilità soprattutto nel condurre al LUG altre persone oltre ai fondatori. Tra questi "nuovi" era presente colui che gentilmente offrì la disponibilità dell'Hacklab di Sesto Calende, la prima sede del gruppo.

In un clima quasi pionieristico, durante gli incontri si parlava sia di aspetti tecnici che di tematiche relative al Pinguino ed ognuno poteva dire la sua.
Si cercava, con fatica e non si sa quanto coscientemente, ma di certo in maniera spontanea, di seguire gli scopi tipici di un qualsiasi Linux User Group, LUG appunto: condividere la conoscenza e la passione per il mondo GNU/Linux.

E questo avveniva senza porsi particolari problematiche di ordine organizzativo... D'altronde, chi aveva mai visto cose come un'Install Fest?! Viene registrato pertanto l'anno 2001 come un anno di assestamento che condusse, nel mese di novembre, alla prima vera iniziativa sul territorio da parte del LinuxVar.

## Il primo LinuxDay

Quel primo [LinuxDay](https://www.linuxday.it), il [LinuxDay 2001 (TODO: controllare collegamento)](/posts/2001/12/linuxday-2001), si svolse nei saloni del comune di Sesto Calende e coinvolse solo un piccolo nucleo di persone, impegnate anche nel trasporto dei personal computer dalla sede al luogo preposto. Bisogna ricordare i tempi in cui muovere un PC era una bella fatica.

Fu comunque una giornata memorabile: nonostante lo scarso tempo a disposizione per la sua organizzazione e la poca pubblicità svolta,
registrammo la presenza (crediamo bene) di una cinquantina di persone e tutto si svolse regolarmente.

L'utilizzo dell'allora innovativo videoproiettore per mostrare l’installazione di GNU/Linux, ci consentì di fare la nostra bella figura. Riflettendo successivamente sull’evento, giungemmo alla conclusione di essere sulla via giusta.

## La crescita e il LIFO

Da quel momento, col gruppo in crescita, si è andata delineando in maniera più chiara la missione fondamentale del LinuxVar: operare in prima persona nell’organizzare e promuovere gli eventi riguardanti GNU/Linux e ciò grazie anche all'incontro con gli amici del [LIFO](https://www.lifolab.org/) (Laboratorio Informatico Free Open).

Il LIFO si occupava principalmente di organizzare corsi su GNU/Linux. Dopo un'iniziale collaborazione col LinuxVar, per evitare sovrapposizione di contenuti, si è deciso di non impegnarsi su quel fronte, lasciando al LIFO l'onere e l'onore.

Nonostante questo, la condivisione degli obiettivi ha condotto LinuxVar e LIFO a preparare insieme il successivo [LinuxDay](https://www.linuxday.it).

## LinuxVar 2002

Il [LinuxDay 2002 (TODO: controllare collegamento)](/posts/2002/11/linuxday-2002) si svolse a Solbiate Olona il 23 novembre 2002 con la collaborazione, in termini di locali offerti, del Comune.

Non possiamo tralasciare i ringraziamenti per l’Agenzia Giovani che contribuì a spianarci la via presso le locali istituzioni.
In quella sede ci furono messi a disposizione diversi locali che utilizzammo per differenziare le attività nell’arco della giornata.

I seminari godettero perciò di un ampio ambiente dotato di videoproiettore (guai se mancasse!) e i nostri stand di una apposita sala. Anche gli
smanettoni del LAN party poterono spremere le loro CPU in un locale isolato. Vi invitiamo a vedere le relative foto disponibili sul nostro sito.

Al termine la soddisfazione fu tanta, quanto l’impegno per tutti noi. Parecchia gente si alternò nella giornata, tuttavia si constatò una scarsa presenza di giovani presso la sala del LAN party. D'altronde GNU/Linux nel settore videoludico si trovava (e si trova ancora purtroppo...) allo stato primordiale e forse siamo stati un po' troppo precursori dei tempi.

Quell'anno si concluse con un incontro pre-natalizio presso la sede del LIFO, dove, tra panettoni e spumante, si parlò piacevolmente delle prospettive per il futuro.

## 2003 e il sito

L'anno 2003 si aprì con l’esigenza, sentita dalla maggioranza, di farsi conoscere più diffusamente: in fondo erano ancora in pochi a conoscere il LUG.

Dopo discussioni si giunse a definire due programmi da sviluppare: il primo riguardante l’aggiornamento, meglio dire il rifacimento, del sito ormai datato e scarno nei contenuti; il secondo la predisposizione di un documento, poi conosciuto come il Manifesto, che definisse lo spirito che ci anima.

In maniera non scientifica alcuni di noi si approcciarono al primo impegno. Utilizzando PHP-Nuke venne creato un primo sito. A causa delle diverse falle di sicurezza insite nel programma e della poca dimestichezza acquisita nel suo utilizzo, venne però avviato anche uno sviluppo parallelo usando Envolution. Quest'ultimo, una volta superata la fase di testing, sostituì definitivamente il precedente.

Si creò anche il logo che ora campeggia nella homepage e che richiama il connubio tra la nostra provincia ed il mondo dell’aviazione. Il dominio `linuxvar.it` (ora `linuxvar.eu`) venne acquistato grazie alle offerte raccolte durante il LinuxDay 2002.

Lo stesso avvenne per il secondo punto, il Manifesto. L’impegno iniziale fu profuso nel definire quali fossero i punti salienti del nostro modus operandi e cosa potesse ben qualificare il LUG.
Raccolte quindi le osservazioni, nei mesi seguenti si andò formando una bozza. Questa fu ampiamente rivista per le numerose critiche ed i nuovi suggerimenti. Nel mese di settembre il Manifesto venne approvato in sede (ovviamente non mancò qualche ulteriore richiesta di piccoli miglioramenti).

## Install Fest e mission

Nel corso dell’anno non ci dimenticammo, tuttavia, di organizzare il nostro primo [Install Fest](/posts/2003/06/install-fest-tradate) con lo scopo di agevolare l’approccio del neofita al sistema operativo GNU/Linux. Si tenne il 29 giugno 2003 presso la Sala Consiliare del Municipio di Tradate.

L'affluenza non fu elevata, vuoi per il caldo che portò le persone più in riva ai laghi che al chiuso, vuoi perché il battage pubblicitario fu volutamente di basso profilo.

Lo scopo di questo tipo di evento, infatti, non è radunare folle oceaniche per decantare le lodi di GNU/Linux, bensì aiutare le persone partecipanti a comprenderne la differente filosofia attraverso il suo utilizzo.
L'esperimento, perché tale era, ci ha consentito di osservare che questa tipologia di eventi è preferibile offrirla nell’arco temporale coincidente col calendario scolastico. Di positivo abbiamo notato che il LUG, tramite questa iniziativa, ha conosciuto nuove adesioni.

Se dovessimo dare ora un giudizio sul nostro operato tra alti e bassi, potremmo dire di aver raggiunto un buon livello di confidenza con la gestione delle iniziative pubbliche. Questo grazie allo spirito cameratesco e rigidamente gratuito che caratterizza i nostri aderenti e soprattutto grazie al loro entusiasmo.

## Il futuro

Da allora non abbiamo mai smesso di seguire e diffondere il verbo del mondo GNU/Linux, pianificando eventi e organizzando anno dopo anno, spesso con la partecipazione di LIFO e [GL-Como](https://www.gl-como.it/), il LinuxDay.
