---
title: Privacy Policy
seo:
  title: Privacy Policy
permalink: /privacy/index.html
description: Noi rispettiamo la EU General Data Protection Regulation (GDPR) e questo documento di policy spiega come collezioniamo e gestiamo le informazioni che ci fornisci.
layout: page
---
Noi rispettiamo la EU General Data Protection Regulation (GDPR) e questo documento di policy spiega come collezioniamo e gestiamo le informazioni che ci fornisci.

**Quali background data vengono collezionati?**
Questo sito non usa alcun tipo di script di tracciamento di terze parti.

**Come usiamo i dati che ci fornisci?**
Semplice, questo sito non colleziona alcun dato.

**Quanto sono al sicuro i tuoi dati?**
Molto al sicuro, in quanto questo sito non colleziona alcuna informazione.

**Quanto è sicuro questo sito?**
Questo sito viene trasmesso usando HTTPS, crittando tutti i dati tra il tuo browser e il server. Il tuo network provider può comunque vedere che stai scaricando materiale da questo sito, ma non il suo contenuto. Questo ti protegge anche da terze parti che cercano di intercettare il tuo traffico, per esempio mentre navighi attraverso WiFi pubbliche or da network provider che iniettano pubblicità su questo sito. Tutti gli adeguati header HTTP sono impostati per la tua protezione e la Content Security Policy è adattata ai requisiti di questo sito.

**Data controller**
{% include "partials/imprint.njk" %}

**Cambiamenti a queste policy**
Se cambiamo il contenuto di questa policy, I cambiamenti saranno effettivi nel momento in cui verranno aggiunti a questa pagina.

**Reclami**
Se hai qualche ragione per dubitare di come gestiamo la tua privacy, ti preghiamo di contattarci via email: <a href="mailto:{{ meta.address.email }}">{{ meta.address.email }}</a>.
