---
title: Chi Siamo
permalink: /about/index.html
description: Chi siamo e cos'è il LinuxVar
layout: page
gallery: true
---

{% image
    "./src/_includes/svg/linuxvar-horizontal.svg",
    "linuxvar logo",
    "Il nostro logo",
    "no-border"
%}

## L'associazione

L'associazione LinuxVar è un LUG, un **L**inux **U**ser **G**roup, della provincia di Varese.

Il LinuxVar raggruppa persone che condividono la filosofia del Software Libero, il [free software](http://www.linux.it/softwarelibero).
Il gruppo si propone di stimolare la conoscenza e la diffusione in ambito locale di [GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.it.html) attraverso il costante confronto tra gli aderenti, grazie alle riunioni aperte del mercoledì sera, e alla creazione di nuova documentazione.

Non viene tralasciata la gestione in prima persona di manifestazioni pubbliche, ad esempio il "classico" [LinuxDay](https://www.linuxvar.it/linuxday/), a fine ottobre, così da dare maggiore risalto all'associazione.

L'Associazione è solo uno dei gruppi presenti in provincia di Varese. Esiste un [elenco](https://lugmap.linux.it/lombardia/) dei gruppi attivi della provincia e della loro attività.

## Costituzione e statuto

Il LinuxVar esiste come gruppo organizzato dal 2001, ma il **4 ottobre 2006** si è costituita ufficialmente come associazione, a Sesto Calende in Viale Italia, 1.

{% gallery %}
[
    {
        "source": "./src/pages/images/associazione/Associazione_99_i_soci_fondatori.jpg",
        "alt": "foto dei soci fondatori",
        "caption": "I soci fondatori"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_98_Il_primo_consiglio_direttivo.jpg",
        "alt": "foto del primo consiglio direttivo",
        "caption": "Il primo consiglio direttivo eletto"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_00_carlo_firma.jpg",
        "alt": "foto di carlo che firma lo statuto",
        "caption": "Carlo firma lo statuto sotto gli occhi di Lionello"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_01_lorenzo_firma.jpg",
        "alt": "foto di lorenzo che firma",
        "caption": "Lorenzo firma"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_16_ujinfan_legge.jpg",
        "alt": "foto di gabriele che legge",
        "caption": "Gabriele legge"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_02_discussione_cash.jpg",
        "alt": "foto di persone che parlano",
        "caption": "Si parla di tecnologia e si presentano i documenti per diventare soci fondatori"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_03_elvio_teo.jpg",
        "alt": "foto di elvio e matteo",
        "caption": "Elvio e Matteo"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_05_ujinfan_esulta.jpg",
        "alt": "foto di grabriele che alza le mani in segno di vittoria",
        "caption": "Gabriele esulta"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_20_spoglio_schede_2.jpg",
        "alt": "foto del conteggio dei voti",
        "caption": "Si contano i voti"
    },
    {
        "source": "./src/pages/images/associazione/Associazione_21_risultati_elezioni.jpg",
        "alt": "foto dei risultati della votazione",
        "caption": "I risultati della votazione"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_1.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_10.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_11.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_12.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_13.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_14.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_15.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_16.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_17.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_19.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_2.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_20.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_21.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_22.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_23.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_24.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_25.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_26.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_27.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_3.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_4.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_5.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_6.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_7.jpg",
        "alt": "foto della serata"
    },
    {
        "source": "./src/pages/images/associazione/LV_Assoc_2006_9.jpg",
        "alt": "foto della serata"
    }
]
{% endgallery %}

### Statuto

Lo statuto del LinuxVar è stato il frutto di lunghi dibattiti e riflessioni. [Dagli un'occhiata](/files/associazione/Statuto_finale.pdf).

### Il Consiglio Direttivo

Il Consiglio Direttivo viene eletto ogni anno tra i soci ed ha il compito di stimolare e coordinare le attività all'interno dell'associazione ed interagire con l’esterno.

### Diventare soci

Diventare socio dell'associazione è molto semplice: vieni a trovarci ad uno degli incontri e consegnaci il [modulo di adesione](/files/associazione/Domanda_soci.pdf).

### Un po' di storia del LinuxVar

Per un bel racconto sulla storia degli inizi del LinuxVar, guarda [qui](/history).

## Contatti

Per quanto riguarda la nostra presenza virtuale, puoi fare riferimento ai link social in fondo alla pagina.

Ci trovi principalmente sul [gruppo Telegram](https://t.me/joinchat/QmTosPpV6Qo9QtyS) su cui ormai si svolgono tutte le comunicazioni.
Se hai bisogno di altre informazioni puoi scrivere al nostro indirizzo mail [{{ meta.address.email }}](mailto:{{ meta.address.email }}).

La nostra piccola, ma accogliente, sede, si trova al nostro indirizzo:

{% include 'partials/imprint.njk' | trim %}

Solitamente ci ritroviamo **tutti i {{ meta.meet.weekday }}**, a partire dalle ore **{{ meta.meet.time }}**. Se desideri prenderne parte, chiedi una conferma della data su Telegram.

{% iframe
    "https://www.openstreetmap.org/export/embed.html?bbox=8.739562332630157%2C45.6709485028323%2C8.74223917722702%2C45.67202619705974&amp;layer=mapnik&amp;marker=45.67148641541454%2C8.740900754928589",
    "Vedi su OpenStreetMaps",
    "https://www.openstreetmap.org/?mlat=45.67149&mlon=8.74090#map=19/45.67149/8.74090"
%}
