# LinuxVar.eu

[LinuxVar.eu](https://linuxvar.eu) website source code.

Project is built using the excellent [Eleventy](https://www.11ty.dev/) and the wonderful starter [Eleventy Excellent](https://github.com/madrilene/eleventy-excellent).
There is also a sprinkle of [Alpine.js](https://alpinejs.dev/).

## Setup

The setup is like any normal Javascript application:

```bash
npm install
```

## Develop

To have a dev server on port 8080, watching for your changes:

```bash
npm start
```

Which in turn launches:

```bash
npx @11ty/eleventy --serve --watch
```

## Build

To build a production ready package:

```bash
npm run build
```

Which in turn launches:

```bash
npx @11ty/eleventy
```

## License

[Eleventy Excellent](https://github.com/madrilene/eleventy-excellent), the starter project which was a base to build this site is licensed under the [ISC](https://opensource.org/license/isc-license-txt/) license.

All subsequent code added by us is licensed under [MIT](https://opensource.org/license/MIT/) license.
