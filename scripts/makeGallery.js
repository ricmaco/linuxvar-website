const fs = require('node:fs/promises');
const path = require('node:path');
const process = require('node:process');

const PROJECT_ROOT = path.dirname(__dirname);

function checkArgs() {
    if (process.argv.length < 3) {
        console.log('usage: makeGallery.js <alt> [<caption>]');
        process.exit(1);
    } 
}

async function walk(dir) {
    const files = await fs.readdir(dir);
    return files
        .map(file => path.join(dir, file))
        .filter(async filePath => {
            const stats = await fs.stat(filePath);
            return !stats.isDirectory();
        })
        .map(filePath => path.relative(PROJECT_ROOT, filePath));
}

function buildGallery(files, alt, caption) {
    const gallery = files
        .map(file => {
            const element = {
                source: `./${file}`,
                alt
            };
            if (caption) element[caption] = caption;
            return element;
        });
    
    const galleryAsStr = JSON.stringify(gallery, null, 4);
    return `{% gallery %}\n${galleryAsStr}\n{% endgallery %}`;
}

(async () => {
    checkArgs();

    const alt = process.argv[2];
    const caption = process.argv[3];

    const files = await walk(process.cwd());
    const gallery = buildGallery(files, alt, caption);

    console.log(gallery);
})();
